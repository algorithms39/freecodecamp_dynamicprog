# AllConstruct
- Write a function 'allConstruct ( target, wordBank) that accepts a
  target string and an array of strings.
- The function should return a 2D array containing all of the ways
  that the target can be constructed by concatenating elements of
  the word Bank" array. Each element of the 2D array should represent
  one combination that constructs the target'.
- You may reuse elements of wordBank° as many times as needed.

# BestSum
- Write a function bestSum( targetSum , numbers) that takes in a
targetSum and an array of numbers as arguments.
- The function should return an array containing the shortest
combination of numbers that add up to exactly the target Sum.
- If there is a tie for the shortest combination, you may return any
one of the shortest.

# CanConstruct
- Write a function 'canConstruct ( target, wordBank)" that accepts a
target string and an array of strings.
- The function should return a boolean indicating whether or not the
"target" can be constructed by concatenating elements of the
"wordBank" array.
- You may reuse elements of wordBank" as many times as needed.

# CanSum
- Write a function canSum(targetSum , numbers) that takes in a
targetSum and an array of numbers as arguments.
- The function should return a boolean indicating whether  it
is possible to generate the targetSum using numbers from the array.
- You may use an element of the array as many times as needed.
- You may assume that all input numbers are nonnegative.

# CountConstruct
- Write a function * count Construct( target, wordBank) that accepts a
  target string and an array of strings.
- The function should return the number of ways that the target" can
  be constructed by concatenating elements of the wordBank" array.
- You may reuse elements of wordBank° as many times as needed.

# Fibonacci_memoization
- Write a function fib(n) " that takes in a number as an argument.
- The function should return the n-th number of the Fibonacci sequence.
- The 1st and 2nd number of the sequence is 1.
- To generate the next number of the sequence, we sum the previous two.

```
n:      1, 2, 3, 4, 5, 6, 7,   8,  9,
fib(n): 1, 1, 2, 3, 5, 8, 13, 21, 34,
```

# GridTraveller
- Say that you are a traveler on a 20D grid. You begin in the
  top-left corner and your goal is to travel to the bottom-right
  corner. You may only move down or right.
- In how many ways can you travel to the goal on a grid with
  dimensions m * n?
- Write a function gridTraveler (m, n) that calculates this.

# HowSum
- Write a function howSum( targetSum , numbers )° that takes in a
targetSum and an array of numbers as arguments.
- The function should return an array containing any combination of
elements that add up to exactly the targetSum. If there is no
combination that adds up to the targetSum, then return null.
- If there are multiple combinations possible, you may return any
single one.

