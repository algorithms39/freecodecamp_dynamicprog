import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Best_Sum {
    public static void main(String[] args) {
        HashMap<Integer, ArrayList<Integer>> memo = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> memo1 = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> memo2 = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> memo3 = new HashMap<>();

        System.out.println(best_sum_memo(7, new int[]{5, 3, 4, 7}, memo));

        System.out.println(best_sum_memo(8, new int[]{2, 3, 5}, memo1));
        System.out.println(best_sum_memo(8, new int[]{2, 4}, memo2));
        System.out.println(best_sum_memo(100, new int[]{2, 5, 25}, memo3));
    }

    public static ArrayList<Integer> best_sum(int targetSum, int[] numbers) {
        if (targetSum == 0) {
            return new ArrayList<Integer>();
        }
        if (targetSum < 0) {
            return null;
        }
        ArrayList<Integer> best = null;

        for (int i = 0; i < numbers.length; i++) {
            ArrayList<Integer> list = best_sum(targetSum - numbers[i], numbers);
            if (list != null) {
                ArrayList<Integer> combination = new ArrayList<>(list);
                combination.add(numbers[i]);
                if (best == null || best.size() > combination.size()) {
                    best = combination;
                }
            }
        }

        return best;

    }

    public static ArrayList<Integer> best_sum_memo(int targetSum, int[] numbers, HashMap<Integer, ArrayList<Integer>> memo) {
        if (memo.containsKey(targetSum)) {
            return memo.get(targetSum);
        }

        if (targetSum == 0) {
            return new ArrayList<Integer>();
        }
        if (targetSum < 0) {
            return null;
        }
        ArrayList<Integer> best = null;

        for (int i = 0; i < numbers.length; i++) {
            ArrayList<Integer> list = best_sum_memo(targetSum - numbers[i], numbers, memo);
            if (list != null) {
                ArrayList<Integer> combination = new ArrayList<>(list);
                combination.add(numbers[i]);
                if (best == null || best.size() > combination.size()) {
                    best = combination;
                }
            }
        }
        memo.put(targetSum, best);
        return best;
    }
//    public static ArrayList<Integer> bestSum(int targetSum, int[] numbers, HashMap<Integer, ArrayList<Integer>> memo) {
//        if (memo.containsKey(targetSum)) {
//            return memo.get(targetSum);
//        }
//
//        if (targetSum == 0) {
//            return new ArrayList<Integer>();
//        }
//        if (targetSum < 0) {
//            return null;
//        }
//        ArrayList<Integer> shortestCombination = null;
//        for (int num : numbers) {
//            int remainder = targetSum - num;
//            ArrayList<Integer> remainderCombination = bestSum(remainder, numbers, memo);
//            if (remainderCombination != null) {
//                ArrayList<Integer> combination = new ArrayList<>(remainderCombination);
//                combination.add(num);
//                if (shortestCombination == null || combination.size() < shortestCombination.size()) {
//                    shortestCombination = combination;
//                }
//            }
//        }
//        memo.put(targetSum, shortestCombination);
//        return shortestCombination;
//    }


}
