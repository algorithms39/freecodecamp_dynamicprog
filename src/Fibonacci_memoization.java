public class Fibonacci_memoization {
    public static void main(String[] args) {
        int n = 100;

        long startTime = System.nanoTime();
        long result1 = fib_iteration(n);
        long endTime = System.nanoTime();
        System.out.println("iteration time: " + (endTime - startTime) + "\n result: " + result1);

        long startTime1 = System.nanoTime();
        long[] memo = new long[n + 1];
        long result2 = fib_memo(n, memo);
        long endTime1 = System.nanoTime();
        System.out.println("memo time: " + (endTime1 - startTime1) + "\n result: " + result2);


    }

    public static long fib_recursive(int number) {
        if (number <= 2) {
            return 1l;
        }
        return fib_recursive(number - 1) + fib_recursive(number - 2);

    }

    public static long fib_memo(int number, long[] memo) {
        if (number == 0) {
            return 0;
        } else if (number <= 2) {
            return 1;
        }
        if (memo[number] != 0) {
            return memo[number];
        }
        memo[number] = fib_memo(number - 1, memo) + fib_memo(number - 2, memo);
        return memo[number];

    }

    // 0, 1, 1, 2, 3, 5, 8, 13, 21
    public static long fib_iteration(int number) {
        long[] prev = {0, 1};

        if (number <= 1) {
            return prev[number];
        }
        long temp = 0;
        for (int l = 1; l < number; l++) {
            temp = prev[1];
            prev[1] = prev[0] + prev[1];
            prev[0] = temp;
        }
        return prev[1];
    }
}
