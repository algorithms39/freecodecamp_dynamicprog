public class Best_Sum_Tabulation {
    public static void main(String[] args) {

        printArr(bestSum(7, new int[]{5, 3, 4, 7}));

        printArr(bestSum(8, new int[]{2, 3, 5}));
        printArr(bestSum(8, new int[]{2, 4}));
        printArr(bestSum(100, new int[]{1,2, 5, 25}));
        printArr(bestSum(100, new int[]{2, 5, 25}));
    }


    public static int[] bestSum(int targetSum, int[] numbers) {
        int[][] dp = new int[targetSum + 1][];
        dp[0] = new int[targetSum + 1];
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                int index = i + numbers[j];
                if (dp[i] != null && index <= targetSum) {
                    if (dp[index] == null || (count(dp[index]) > count(dp[i]) + 1)) {
                        dp[index] = dp[i].clone();
                        dp[index][index] = numbers[j];

                    }
                }
            }
        }

        if (dp[targetSum] != null) {
            return dp[targetSum];
        } else {
            return dp[0];
        }

    }

    public static void printArr(int[] arr) {
        System.out.print("[");

        for (int j = 0; j < arr.length; j++) {
            if (arr[j] != 0) {
                System.out.print(arr[j]);
                if (j + 1 != arr.length) {
                    System.out.print(", ");
                }
            }

        }
        System.out.print("]");
        System.out.println();

    }

    public static int count(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                count++;
            }
        }
        return count;
    }
}