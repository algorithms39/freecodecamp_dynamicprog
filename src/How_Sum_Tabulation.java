import java.util.ArrayList;

public class How_Sum_Tabulation {
    public static void main(String[] args) {
        printArr(howSum(7, new int[]{2, 3}));
        printArr(howSum(7, new int[]{5, 3, 4, 7}));
        printArr(howSum(7, new int[]{2, 4}));
        printArr(howSum(8, new int[]{2, 3, 5}));
        printArr(howSum(300, new int[]{7, 14}));
    }

    public static int[] howSum(int targetSum, int[] numbers) {
        int[][] dp = new int[targetSum + 1][];
        dp[0] = new int[targetSum + 1];

        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                int index = i + numbers[j];

                if (dp[i] != null & index <= targetSum) {
                    dp[index] = dp[i].clone();
                    dp[index][index] = numbers[j];
                }
            }
        }
        if (dp[targetSum] != null) {
            return dp[targetSum];

        } else {
            return dp[0];
        }
    }

    public static void printArr(int[] arr) {
        System.out.print("[");

        for (int j = 0; j < arr.length; j++) {
            if (arr[j] != 0) {
                System.out.print(arr[j]);
                if (j + 1 != arr.length) {
                    System.out.print(", ");
                }
            }

        }
        System.out.print("]");
        System.out.println();
    }
}
