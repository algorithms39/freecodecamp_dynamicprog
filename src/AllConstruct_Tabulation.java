import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AllConstruct_Tabulation {
    public static void main(String[] args) {
        System.out.println(allConstruct("purple", new String[]{"purp", "p", "ur", "le", "purpl"}));
        System.out.println(allConstruct("abcdef", new String[]{"ab", "abc", "cd", "def", "abcd"}));
        System.out.println(allConstruct("skateboard", new String[]{"bo", "rd", "ate", "t", "ska", "sk", "boar"}));
        System.out.println(allConstruct("enterapotentpot", new String[]{"a", "p", "ent", "enter", "ot", "o", "t"}));
        System.out.println(allConstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{"e", "ee", "eee", "eeee", "eeeee", "eeeeee",})); //
    }

    public static List allConstruct(String target, String[] wordBank) {
        int n = target.length();
        ArrayList<ArrayList<String>>[] table = new ArrayList[n + 1];

        table[0] = new ArrayList<ArrayList<String>>();
        table[0].add(new ArrayList<>());


        for (int i = 0; i <= n; i++) {
            if (table[i] != null) {
                for (int j = 0; j < wordBank.length; j++) {
                    int range = i + wordBank[j].length();
                    if (range <= n && target.substring(i, range).equals(wordBank[j])) {
                        ArrayList<ArrayList<String>> list = new ArrayList<>(table[i].stream().map(x -> new ArrayList<>(x)).collect(Collectors.toList()));
                        for (int k = 0; k < list.size(); k++) {
                            list.get(k).add(wordBank[j]);

                            if (table[range] != null) {
                                table[range].add(list.get(k));

                            }

                        }
                        if (table[range] == null) {
                            table[range] = list;

                        }

                    }
                }

            }
        }

        return table[n];
    }


}
