import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class AllConstruct {
    public static void main(String[] args) {
        HashMap<String, Integer> memo = new HashMap<>();

        System.out.println(allConstruct("purple", Arrays.asList("purp", "p", "ur", "le", "purpl")));
        System.out.println(allConstruct("abcdef", Arrays.asList("ab", "abc", "cd", "def", "abcd")));
        System.out.println(allConstruct("skateboard", Arrays.asList("bo", "rd", "ate", "t", "ska", "sk", "boar")));
        System.out.println(allConstruct("enterapotentpot", Arrays.asList("a", "p", "ent", "enter", "ot", "o", "t")));
//            System.out.println(allConstruct_memo("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{
//                    "e",
//                    "ee",
//                    "eee",
//                    "eeee",
//                    "eeeee",
//                    "eeeeee",
//            }, memo)); // false


    }

    public static List<List<String>> allConstruct(String target, List<String> wordBank) {
        if (target.isEmpty()) return new ArrayList<>();

        List<List<String>> result = new ArrayList<>();
        for (String word : wordBank) {
            if (target.indexOf(word) == 0) {
                String suffix = target.substring(word.length());
                List<List<String>> suffixWays = allConstruct(suffix, wordBank);
                for (List<String> way : suffixWays) {
                    List<String> targetWays = new ArrayList<>();
                    targetWays.add(word);
                    targetWays.addAll(way);
                    result.add(targetWays);
                }
            }
        }

        return result;
    }

}
