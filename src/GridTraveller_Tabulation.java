public class GridTraveller_Tabulation {
    public static void main(String[] args) {
        System.out.println(gridTraveller(2, 3));
        System.out.println(gridTraveller(3, 2));
        System.out.println(gridTraveller(3, 3));
        System.out.println(gridTraveller(18, 18));
    }

    public static long gridTraveller(int r, int c) {
        long[][] dp = new long[r + 1][c + 1];
        dp[1][1] = 1;

        for (int i = 0; i <= r; i++) {
            for (int j = 0; j <= c; j++) {
                long current = dp[i][j];
                if (j + 1 <= c) {
                    dp[i][j + 1] += current;
                }
                if (i + 1 <= r) {
                    dp[i + 1][j] += current;
                }
            }
        }
        return dp[r][c];
    }
}
