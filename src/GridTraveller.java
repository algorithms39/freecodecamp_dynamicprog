import java.util.HashMap;

public class GridTraveller {
    public static void main(String[] args) {
        //row and column
        int r = 20;
        int c = 30;
        int[][] memo = new int[r + 1][c + 1];
        HashMap<String, Integer> memo_hash = new HashMap<>();

        long start2 = System.nanoTime();
        int result2 = gridTravellerMemo(r, c, memo);
        long end2 = System.nanoTime();
        System.out.println("dynamic time table" + (end2 - start2) + " result : " + result2);

        long start1 = System.nanoTime();
        int result1 = gridTravellerMemo_withHash(r, c, memo_hash);
        long end1 = System.nanoTime();
        System.out.println("hash time" + (end1 - start1) + " result : " + result1);


    }

    public static int gridTravellerRecursive(int r, int c) {
        if (c == 0 || r == 0) {
            return 0;
        } else if (c == 1 && r == 1) {
            return 1;
        }
        //go down
        int down = gridTravellerRecursive(c - 1, r);
        //go right
        int right = gridTravellerRecursive(c, r - 1);
        return down + right;
    }

    public static int gridTravellerMemo(int r, int c, int[][] memo) {
        if (c == 0 || r == 0) {
            return 0;
        } else if (c == 1 && r == 1) {
            return 1;
        }
        if (memo[r][c] != 0) {
            return memo[r][c];
        }

        memo[r][c] = gridTravellerMemo(r - 1, c, memo) + gridTravellerMemo(r, c - 1, memo);

        return memo[r][c];
    }

    public static int gridTravellerMemo_withHash(int r, int c, HashMap<String, Integer> memo) {
        String key = r + "," + c;
        String key_reverse = c + "," + r;
        if (c == 0 || r == 0) {
            return 0;
        } else if (c == 1 && r == 1) {
            return 1;
        }
        if (memo.containsKey(key)) {
            return memo.get(key);
        } else if (memo.containsKey(key_reverse)) {
            return memo.get(key_reverse);
        }
        memo.put(key, gridTravellerMemo_withHash(r - 1, c, memo) + gridTravellerMemo_withHash(r, c - 1, memo));

        return memo.get(key);
    }

}
