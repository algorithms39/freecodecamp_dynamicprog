import java.util.HashMap;

public class CanConstruct {
    public static void main(String[] args) {
        long starttime1 = System.nanoTime();
//        System.out.println(canConstruct("abcdef", new String[]{"ab", "abc", "cd", "def", "abcd"}));
//        System.out.println(canConstruct("skateboard", new String[]{"bo", "rd", "ate", "t", "ska", "sk", "boar"}));
//        System.out.println(canConstruct("enterapotentpot", new String[]{"a", "p", "ent", "enter", "ot", "o", "t"}));
//        System.out.println(canConstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{
//                "e",
//                "ee",
//                "eee",
//                "eeee",
//                "eeeee",
//                "eeeeee",
//        })); // false
        long endtime1 = System.nanoTime();
        HashMap<String, Boolean> memo = new HashMap<>();
        HashMap<String, Boolean> memo2 = new HashMap<>();
        HashMap<String, Boolean> memo3 = new HashMap<>();
        HashMap<String, Boolean> memo4 = new HashMap<>();

        long starttime2 = System.nanoTime();
        System.out.println(canConstruct_memo("abcdef", new String[]{"ab", "abc", "cd", "def", "abcd"}, memo));
        System.out.println(canConstruct_memo("skateboard", new String[]{"bo", "rd", "ate", "t", "ska", "sk", "boar"}, memo2));
        System.out.println(canConstruct_memo("enterapotentpot", new String[]{"a", "p", "ent", "enter", "ot", "o", "t"}, memo3));
        System.out.println(canConstruct_memo("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{
                "e",
                "ee",
                "eee",
                "eeee",
                "eeeee",
                "eeeeee",
        }, memo4)); // false
        long endtime2 = System.nanoTime();


        System.out.println("canConstructTime: " + (endtime1 - starttime1));
        System.out.println("canConstructmemoTime: " + (endtime2 - starttime2));
    }

    public static boolean canConstruct(String target, String[] wordBank) {
        if (target.length() == 0) {
            return true;
        }
        for (String word : wordBank) {
            if (target.indexOf(word) == 0) {
                String subString = target.substring(word.length());


                boolean check = canConstruct(subString, wordBank);
                if (check) {
                    return true;
                }
            }

        }

        return false;
    }

    public static boolean canConstruct_memo(String target, String[] wordBank, HashMap<String, Boolean> memo) {
        if (memo.containsKey(target)) {
            return memo.get(target);
        }
        if (target.length() == 0) {
            return true;
        }
        for (String word : wordBank) {
            if (target.indexOf(word) == 0) {

                String subString = target.substring(word.length());


                boolean check = canConstruct_memo(subString, wordBank, memo);
                if (check) {
                    memo.put(target, true);
                    return true;
                }
            }

        }
        memo.put(target, false);
        return false;
    }

}

