import java.util.HashMap;

public class CountConstruct {

    public static void main(String[] args) {
        HashMap<String, Integer> memo = new HashMap<>();

        System.out.println(countConstruct("purple", new String[]{"purp", "p", "ur", "le", "purpl"}));
        System.out.println(countConstruct("abcdef", new String[]{"ab", "abc", "cd", "def", "abcd"}));
        System.out.println(countConstruct("skateboard", new String[]{"bo", "rd", "ate", "t", "ska", "sk", "boar"}));
        System.out.println(countConstruct("enterapotentpot", new String[]{"a", "p", "ent", "enter", "ot", "o", "t"}));
        System.out.println(countConstruct_memo("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{
                "e",
                "ee",
                "eee",
                "eeee",
                "eeeee",
                "eeeeee",
        }, memo)); // false


    }

    public static int countConstruct(String target, String[] wordBank) {
        if (target.length() == 0) {
            return 1;
        }
        int numWays = 0;
        for (String word : wordBank) {
            if (target.indexOf(word) == 0) {

                String subString = target.substring(word.length());

                int check = countConstruct(subString, wordBank);
                numWays += check;
            }


        }

        return numWays;
    }

    public static int countConstruct_memo(String target, String[] wordBank, HashMap<String, Integer> memo) {
        if (memo.containsKey(target)) {
            return memo.get(target);
        }
        if (target.length() == 0) {
            return 1;
        }
        int numWays = 0;
        for (String word : wordBank) {
            if (target.indexOf(word) == 0) {

                String subString = target.substring(word.length());


                int check = countConstruct_memo(subString, wordBank, memo);
                numWays += check;
            }

        }
        memo.put(target, numWays);
        return numWays;
    }


}
