import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class How_Sum {
    public static void main(String[] args) {
        HashMap<Integer, ArrayList<Integer>> memo = new HashMap<>();

        System.out.println(how_sum(7, new int[]{2, 3}));
        System.out.println(how_sum(7, new int[]{5, 3, 4, 7}));
        System.out.println(how_sum(7, new int[]{2, 4}));
        System.out.println(how_sum(8, new int[]{2, 3, 5}));
        System.out.println(how_sum_memo(300, new int[]{7, 14}, memo));
    }

    //time : O(n^m * m);
    public static ArrayList<Integer> how_sum(int targetSum, int[] numbers) {
        if (targetSum == 0) {
            return new ArrayList<Integer>();
        }
        if (targetSum < 0) {
            return null;
        }

        for (int i = 0; i < numbers.length; i++) {
            ArrayList<Integer> list = how_sum(targetSum - numbers[i], numbers);
            if (list != null) {
                list.add(numbers[i]);
                return list;
            }
        }

        return null;

    }

    //
    public static ArrayList<Integer> how_sum_memo(int targetSum, int[] numbers, HashMap<Integer, ArrayList<Integer>> memo) {
        if (targetSum == 0) {
            return new ArrayList<Integer>();
        }
        if (targetSum < 0) {
            return null;
        }
        if (memo.containsKey(targetSum)) {
            return memo.get(targetSum);
        }

        for (int i = 0; i < numbers.length; i++) {
            ArrayList<Integer> list = how_sum_memo(targetSum - numbers[i], numbers, memo);
            if (list != null) {
                list.add(numbers[i]);
                memo.put(targetSum, list);
                return list;
            }
        }
        memo.put(targetSum, null);
        return null;

    }


}
