public class CountConstruct_Tabulation {
    public static void main(String[] args) {
        System.out.println(countConstruct("purple", new String[]{"purp", "p", "ur", "le", "purpl"}));
        System.out.println(countConstruct("abcdef", new String[]{"ab", "abc", "cd", "def", "abcd"}));
        System.out.println(countConstruct("skateboard", new String[]{"bo", "rd", "ate", "t", "ska", "sk", "boar"}));
        System.out.println(countConstruct("enterapotentpot", new String[]{"a", "p", "ent", "enter", "ot", "o", "t"}));
        System.out.println(countConstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{"e", "ee", "eee", "eeee", "eeeee", "eeeeee",})); // false

    }

    public static int countConstruct(String target, String[] wordBank) {
        int n = target.length();
        int[] table = new int[n + 1];
        table[0] = 1;


        for (int i = 0; i < n + 1; i++) {
            if (table[i] != -1) {
                for (int j = 0; j < wordBank.length; j++) {
                    int range = i + wordBank[j].length();
                    if (range <= n && target.substring(i, range).equals(wordBank[j])) {
                        table[range] += table[i];
                    }
                }
            }
        }

        return table[n];
    }
}
