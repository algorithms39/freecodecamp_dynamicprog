public class Can_Sum {
    public static void main(String[] args) {
        String[] memo = new String[301];
        System.out.println(can_sum(7, new int[]{2, 3}));
        System.out.println(can_sum(7, new int[]{5, 3, 4, 7}));
        System.out.println(can_sum(7, new int[]{2, 4}));
        System.out.println(can_sum(8, new int[]{2, 3, 5}));
        System.out.println(can_sum_memo(300, new int[]{7, 14}, memo));
    }

    public static boolean can_sum(int targetSum, int[] numbers) {
        if (targetSum == 0) {
            return true;
        }
        if (targetSum < 0) {
            return false;
        }

        for (int i = 0; i < numbers.length; i++) {
            boolean check = can_sum(targetSum - numbers[i], numbers);
            if (check) return true;
        }

        return false;

    }

    public static boolean can_sum_memo(int targetSum, int[] numbers, String[] memo) {
        if (targetSum == 0) {
            return true;
        }
        if (targetSum < 0) {
            return false;
        }
        if (memo[targetSum] != null) {
            return Boolean.parseBoolean(memo[targetSum]);
        }

        for (int i = 0; i < numbers.length; i++) {
            boolean check = can_sum_memo(targetSum - numbers[i], numbers, memo);
            memo[targetSum] = String.valueOf(check);
            if (check) return true;
        }
        return false;


    }
}
