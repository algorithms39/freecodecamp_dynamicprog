public class CanConstruct_Tabulation {
    public static void main(String[] args) {
        System.out.println(canConstruct("abcdef", new String[]{"ab", "abc", "cd", "def", "abcd"}));
        System.out.println(canConstruct("skateboard", new String[]{"bo", "rd", "ate", "t", "ska", "sk", "boar"}));
        System.out.println(canConstruct("enterapotentpot", new String[]{"a", "p", "ent", "enter", "ot", "o", "t"}));
        System.out.println(canConstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef", new String[]{
                "e",
                "ee",
                "eee",
                "eeee",
                "eeeee",
                "eeeeee",
        })); // false
    }

    public static boolean canConstruct(String target, String[] wordBank) {
        boolean[] dp = new boolean[target.length() + 1];
        dp[0] = true;

        for (int i = 0; i < dp.length; i++) {
            if (dp[i] == true) {
                for (int j = 0; j < wordBank.length; j++) {
                    // if the word matches the characters starting at position i;
                    int range = i + wordBank[j].length();
                    if (range <= target.length() && target.substring(i, range).equals(wordBank[j])) {
                        dp[range] = true;
                    }
                }
            }
        }

        return dp[target.length()];
    }
}
