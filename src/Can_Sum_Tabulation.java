public class Can_Sum_Tabulation {
    public static void main(String[] args) {
        System.out.println(canSum(7, new int[]{2, 3}));
        System.out.println(canSum(7, new int[]{5, 3, 4, 7}));
        System.out.println(canSum(7, new int[]{2, 4}));
        System.out.println(canSum(8, new int[]{2, 3, 5}));
        System.out.println(canSum(300, new int[]{7, 14}));
    }

    public static boolean canSum(int targetSum, int[] numbers) {
        boolean[] dp = new boolean[targetSum + 1];
        dp[0] = true;

        for (int i = 0; i <= targetSum; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (dp[i] && i + numbers[j] <= targetSum) {
                    dp[i + numbers[j]] = true;
                }
            }
        }
        return dp[targetSum];
    }
}
